var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var mysql      = require('mysql');
var log = [];
var log_init = false;

app.get('/', function(req, res){
  res.sendfile('index.html');
});



var db = {
    connect:function(){
        // TODO: 
        // Cambiar contraseñas de mysql
        var connection = mysql.createConnection({
                              host     : 'localhost',
                              user     : 'root',
                              password : '',
                              database: 'dev'
                            });
        connection.connect(function(err){
            if (err) console.log(err)
        });
        return connection;
    },
    all_messages: function(con, socket){
         var r = function() {
            socket.emit("complete_log", log);
         }
         var save_data = function (data){
            log.push(data);
         }
        con.query("select users.username, users.avatar, tablon.message, tablon.created_at from tablon " +
            "join users on users.id = tablon.user_id " +
            "order by created_at desc limit 30; ")
        .on("result", save_data)
        .on("end",r);

    },
    create_msg: function(con,user_id, msg){
        // TODO:
        // validar y sanitizar msg;
        con.query("insert into tablon set ?",{user_id:user_id, message:msg});
    }
}


function disconnect(){
    console.log("disconnect");
}

function on_message(response){
    console.log("on message "+ response.user.username + " :"  + response.msg); 
    var user = response.user.username;
    var user_id = response.user.id;
    var userenc = response.user.userenc;
    var msg = response.msg;
    //TODO:
    // validar con userenc
    var data = {username:user, message: msg, created_at:""};
    log.push(data);
    io.sockets.emit("tablon", data);
    con = db.connect()
    db.create_msg(con,user_id, msg);
}

io.on('connection', function(socket){
    console.log('a user connected');
    socket.on('disconnect',disconnect);
    socket.on('tablon', on_message);
    if (!log_init) {
        con = db.connect()
        db.all_messages(con,socket);
        log_init = true;
    } else {
        socket.emit("complete_log", log);
    }
    
});

//socket server
http.listen(8001, function(){
  console.log('listening on *:8001');
});