-- --------------------------------------------------------


drop table if exists tablon ;
drop table if exists users ;

--
-- Estructura de tabla para la tabla `users`
--
CREATE TABLE IF NOT EXISTS `tablon` (
`id` bigint(20) NOT NULL AUTO_INCREMENT,
`user_id` bigint(20) NOT NULL,
 `message` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
 created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
 PRIMARY KEY (`id`),
 FOREIGN KEY (user_id) REFERENCES users(id)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;



CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `oauth_uid` text COLLATE utf8_unicode_ci NOT NULL,
  `oauth_provider` text COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `registered` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `role` int(11) NOT NULL,
  `avatar` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `sexo` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `nombre` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `apellido` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `pais` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `postal` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `fecha_nacimiento` text COLLATE utf8_unicode_ci NOT NULL,
  `biography` text COLLATE utf8_unicode_ci NOT NULL,
  `privacidad` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'public',
  `followers` bigint(11) NOT NULL,
  `following` bigint(11) NOT NULL,
  `paso` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `oauth_uid`, `oauth_provider`, `username`, `password`, `email`, `registered`, `role`, `avatar`, `sexo`, `nombre`, `apellido`, `pais`, `postal`, `fecha_nacimiento`, `biography`, `privacidad`, `followers`, `following`, `paso`) VALUES
(1, '', '', 'darze', '2baf86234c5fd0df14bb87faa9f9a9a3', 'darze@megafox.tv', '2015-05-29 04:55:00', 4, 'http://cs623723.vk.me/v623723403/360ed/KJ5dwHnBReA.jpg', 'm', 'Darze', '', 'CO', '04562', '1993-10-17', '...', 'only_following', 1, 2, 3),
(2, '', '', 'atreyus', '43acf80690c47dc0b1d9fda275985774', 'tony3081@megafox.tv', '2015-06-01 03:13:44', 3, '', 'm', 'Atreyus', '', 'MX', '02860', '1988-01-30', '', 'public', 0, 0, 3),
(3, '', '', 'lucy', '4a1543405f5e909be96db8af167c3417', 'lucy@megafox.tv', '2015-06-01 05:13:41', 4, '', 'f', 'Lucy', '', 'MX', '02860', '1990-10-17', '', 'public', 0, 0, 3),
(4, '', '', 'daniel', 'f39e29abcb1d79f86c30f1ce0f22fa7c', 'daniel@@megafox.tv', '2015-06-16 07:38:24', 2, '', 'm', 'Daniel', '', 'MX', '02860', '1992-10-01', '', 'public', 0, 0, 3),
(5, '', '', 'lewis', '95e65aaba675be71616c4c1e1a8a3274', 'lewis@megafox.tv', '2015-06-16 15:54:29', 2, '', 'm', 'Lewis', '', 'MX', '02860', '1997-09-05', '', 'public', 1, 4, 3),
(6, '', '', 'minny3', 'c8f5b88cd810f69737e21c6e7e496e18', 'minny-3@hotmail.es', '2015-06-19 05:46:06', 0, '', 'f', 'Anastasia', 'Lopez zamora', 'ES', '1940', '1994-01-30', '', 'public', 0, 0, 3),
(7, '832244583511952', 'facebook', 'mefloju-15', 'e32f4c3c3f1e9a2d6cbb5d17b7dd77a7', 'mefloju@hotmail.com', '2015-06-19 09:13:20', 0, '', '', 'Melissa', 'Flores Justiniano', '', '', '', '', 'public', 0, 0, 3),
(8, '', '', 'arzolarafael123', '92d233956e09169babc7f9b93e29221f', 'rafael123@hotmail.com', '2015-06-19 10:40:22', 0, '', 'm', 'Rafael', 'Vargas Arzola', 'VE', '0235', '1996-03-16', '', 'public', 0, 0, 3),
(9, '', '', 'africalubian', 'fd17e0b21164ff3dd28a251e4ac67cc5', 'africalubian@gmail.com', '2015-06-19 10:45:49', 0, '', 'f', 'africa', 'lub diaz', 'ES', '32202', '1990-03-12', '', 'public', 0, 0, 3),
(10, '', '', 'andros', '38264e43b1046080cad1f8e890317efe', 'andros@hotmail.com', '2015-06-19 10:50:24', 0, '', 'f', 'Akire', 'Andros', 'MX', '15400', '1983-06-9', '', 'public', 0, 0, 3),
(11, '', '', 'celia2001', 'a19facd223de27222e3ad05887f7533a', 'celia2001@hotmail.com', '2015-06-19 11:01:54', 0, '', 'f', 'Celia', 'Gonzalez reino', 'ES', '06011', '1992-01-10', '', 'public', 0, 0, 3),
(12, '', '', 'loligalan', '256817d9ffdf93ab8076d0af98680a26', 'loligalan@gmail.com', '2015-06-19 11:13:07', 0, '', 'f', 'dolores', 'galan', 'ES', '41013', '1983-07-13', '', 'public', 0, 0, 3),
(13, '', '', 'geinny-bella', '3c5ddc22fe888df3ac15b09a0b2a5aea', 'geinny-bella@hotmail.com', '2015-06-19 11:14:04', 0, '', '', '', '', '', '', '', '', 'public', 0, 0, 3),
(14, '', '', 'agaricus', '546a81a043b84d47ee827f6466990971', 'agaricus@hotmail.com', '2015-06-19 11:41:26', 0, '', 'm', 'Aron', 'Palomares', 'MX', '', '67-07-27', '', 'public', 0, 0, 3),
(15, '', '', 'patrirg611', '8f727b5e1c6fb36d1f2f5a12be826464', 'patrirg611@gmail.com', '2015-06-19 12:04:38', 0, '', 'f', 'patri', 'romero garcia', 'ES', '41740', '1995-11-6', '', 'public', 0, 0, 3),
(16, '', '', 'mariel-jeffhardy', '9c625dd3f789afe2938622daa6b38b92', 'mariel_jeffhardy@hotmail.com', '2015-06-19 12:13:05', 0, '', '', '', '', '', '', '', '', 'public', 0, 0, 3),
(17, '10153511308415865', 'facebook', 'vacaloca1381', 'f1f4221b4bc4f24f573a66c66bac531c', 'vacaloca@hotmail.com', '2015-06-19 12:29:53', 0, '', '', 'Elizabeth', 'Ozano', 'SE', '', '1972-06-22', '', 'public', 0, 0, 3);


INSERT INTO `tablon` (user_id, message)   values 
(1, "holaaaa"),
(2, " ey holaaaa");