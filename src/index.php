<!doctype html>
<html>
  <head>
    <title>tablon</title>
    <style>
      * { margin: 0; padding: 0; box-sizing: border-box; }
      body { font: 13px Helvetica, Arial; }
      form { background: #000; padding: 3px; position: fixed; bottom: 0; width: 100%; color:white; }
      form input { border: 0; padding: 10px; width: 90%; margin-right: .5%; }
      form button { width: 9%; background: rgb(130, 224, 255); border: none; padding: 10px; }
      #messages { list-style-type: none; margin: 0; padding: 0; }
      #messages li { padding: 5px 10px; }
      #messages li:nth-child(odd) { background: #eee; }
    </style>
  </head>
  <body>
    <ul id="messages"></ul>
    <form action="">
        <span id="username"></span>
        <input id="m" autocomplete="off" />
        <button>Send</button>
    </form>
<!-- El socket server despacha el cliente -->
<script src="http://localhost:8001/socket.io/socket.io.js"></script>
<script src="http://code.jquery.com/jquery-1.11.1.js"></script>
<script>
  var socket = io("ws://localhost:8001/");
   var user = {
    username: "<?php echo 'lucy'; // $_SESSION["username"]?>",
    id: <?php echo 3; //$_SESSION["user_id"]?>,
    userenc: "<?php echo 'aUOIUOUI'; //$_SESSION["userenc"] ?>"
   }


   $("#username").text(user.username);
   function append_msg(data) {
    var username = data.username;
    var msg = data.message;
    var date = data.created_at;
     $('#messages').prepend($('<li>').text(username+":"+msg));

   }
   socket.on("tablon", function(data){
    append_msg(data);
   });

   socket.on("complete_log", function(data){
    console.log("complete_log", data)
     $.each(data,function(index,obj){
            append_msg(obj);
        })

   });

  $('form').submit(function(){
    socket.emit("tablon", {
        user: user,
        msg:$('#m').val()
    });
    $('#m').val('');
    return false;
  });
</script>

  </body>
</html>